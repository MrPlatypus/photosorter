#### What is this ?

PhotoSorter is a extremely basic utility to organise my pictures in the way that I enjoy.<br/>

#### Build

###### Windows

All you need to build is a **x64 Native Tools Command Prompt for VS** (tested with 2017, 2019, 2022) and call **build.bat**.
This will generate a non optimised executable with debug symbols. If you want an optimised build, then replace "/Od" with "/O2", in build.bat.

#### How to use ?

PhotoSorter.exe --pictures_path D:/DCIM/ --destination_path C:/Users/ABC/Dest/ [--move]

--pictures_path: The path to where your pictures are stored, directories are also searched recursively just in case you have subfolders.

--destination_path: Where do you want your pictures to end up.

--move: By default your pictures are copied and not moved just in case you wish to keep the originals where they are. But if you specify --move then they will be moved instead.