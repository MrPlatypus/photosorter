#pragma once

#include <stddef.h> // size_t, ptrdiff_t
#include <stdint.h> // int8_t, int16_t, int32_t, int64_t, intptr_t, uint8_t, uint16_t, uint32_t, uint64_t, 

#pragma region TYPES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

typedef int8_t  i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float  f32;
typedef double f64;

typedef size_t    sz;
typedef intptr_t  ptr;
typedef ptrdiff_t ptrdiff;

typedef uint16_t wchar;

#if !defined(__cplusplus)
typedef _Bool bool;
#define false 0
#define true  1
#endif

#pragma endregion

#pragma region MACROS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

#define KILOBYTES(Value) ((Value) * 1024LL)
#define MEGABYTES(Value) (KILOBYTES(Value) * 1024LL)
#define GIGABYTES(Value) (MEGABYTES(Value) * 1024LL)
#define TERABYTES(Value) (GIGABYTES(Value) * 1024LL)

#pragma endregion

#pragma region API - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#if defined(__cplusplus)
extern "C"
{
#endif

/// @brief Specify what memory allocators should be used by the toolbox library.
/// @param Malloc  A function pointer matching malloc's signature.
/// @param Realloc A function pointer matching realloc's signature.
/// @param Free    A function pointer matching free's signature.
void ToolboxSetAllocators(void* (*Malloc)(sz), void* (*Realloc)(void*, sz), void (*Free)(void*));

#if defined(__cplusplus)
}
#endif

#pragma endregion