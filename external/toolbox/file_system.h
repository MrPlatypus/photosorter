#pragma once

#include "toolbox.h"

#pragma region TYPES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

typedef enum e_file_system_result {
	FS_SUCCESS = 0,
	FS_FAILURE,
	FS_FILE_NOT_FOUND,
	FS_DIR_NOT_FOUND,
	FS_INVALID_PATH,
	FS_INVALID_WILDCARD_PATTERN,
	FS_INVALID_PATH_ENDING,
	FS_STRUCT_ZERO_OUT,
} e_file_system_result;

typedef enum e_file_system_search_mode
{
	FS_SEARCH_MODE_FILE = 0,
	FS_SEARCH_MODE_DIRECTORY,
} e_file_system_search_mode;

typedef struct s_file_system_file
{
	u8*   const data;
	sz    const size;
} s_file_system_file;

// All members of this struct are const as they are interdependant and tweaking one may invalidate another.
typedef struct s_file_system_search
{
    void* const                 internal;
	char  const * const         directoryPath;
    char  const * const * const fileNames;
	u32   const * const         fileNameLengths;
    u32   const                 fileCount;
} s_file_system_search;

#pragma endregion

#pragma region API - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#if defined(__cplusplus)
extern "C"
{
#endif

/// @brief Loads the contents of a file in memory.
/// @param file The .data and .size params of the struct will contain the contents of the file at the moment when this function was called. Note that .data will also be null terminated.
/// @return FS_SUCCESS if everything went well. The enum  can be turned into a human readable string with FileSystemResultToString().
e_file_system_result FileSystemRead(s_file_system_file* file, char const * path);

/// @brief Frees all memory allocated by FileSystemRead()
/// @param read The struct that was filled by a previous call to FileSystemRead()
void FileSystemReadFree(s_file_system_file* file);

/// @brief Writes to the file.
/// @param file Must contain a valid handle. The .data and .size params of the struct will not be updated to reflect the new contents of the file.
/// @param data A pointer to the memory that will be written out to the file.
/// @param size The amount of bytes that will be written out to the file.
/// @return FS_SUCCESS if everything went well. The enum  can be turned into a human readable string with FileSystemResultToString().
e_file_system_result FileSystemWrite(char const * path, u8 const * data, sz size);

e_file_system_result FileSystemTimestamp(i64* timestamp, char const * path);

e_file_system_result FileSystemMove(char const * destPath, char const * srcPath); // WIP
e_file_system_result FileSystemCopy(char const * destPath, char const * srcPath); // WIP

/// @brief Searches a directory and fills the user-supplied struct with the result(s).
/// @param search        A zero-ed out struct. Will be filled with the results of the search. If an error occures it will be left untouched.
/// @param directoryPath A valid directory path. OK: "./", "C:/Windows/System32/" KO: ".\", C:/Windows/System32".
/// @param searchPattern Can be a complete filename or a wildcard pattern containing a single '*'. ex: "main.c", "*.c", "file_system.*.c".
/// @param mode          Specify if you are searching for files (FS_SEARCH_MODE_FILE) or directories (FS_SEARCH_MODE_DIRECTORY).
/// @param Filter        [Optional] Custom filtering rules. A function pointer that receives 2 params: a string (representing the file name) and it's length. Returning false excludes the file from the results.
/// @return FS_SUCCESS if everything went well. The enum can be turned into a human readable string with FileSystemResultToString().
e_file_system_result FileSystemSearch(s_file_system_search* search, char const * directoryPath, char const * searchPattern, e_file_system_search_mode mode, bool (*Filter)(char const *, u32));

/// @brief Frees the memory allocated by FileSystemSearch.
/// @param search The s_file_system_search modified by FileSystemSearch.
void FileSystemSearchFree(s_file_system_search* search);

/// @brief 
/// @param result The result returned by most FileSystem functions.
/// @return A string that can be used for error reporting ot the user.
char const * FileSystemResultToString(e_file_system_result result);

#if defined(__cplusplus)
}
#endif

#pragma endregion