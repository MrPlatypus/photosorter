#include <toolbox/toolbox.h>
#include <toolbox/file_system.h>

#include <malloc.h> // alloca, free, malloc, realloc
#include <stddef.h> // size_t, ptrdiff_t
#include <stdint.h> // int8_t, int16_t, int32_t, int64_t, intptr_t, uint8_t, uint16_t, uint32_t, uint64_t, 
#include <stdio.h> // fprintf

#define _AMD64_ // Required when including Window's system headers without first including windows.h.
#define WIN32_LEAN_AND_MEAN // Reduces time spent including certain of Window's headers.
#include <ConsoleApi.h> // GetConsoleOutputCP
#include <ConsoleApi2.h> // SetConsoleOutputCP
#include <fileapi.h> // CreateDirectoryW, CreateFileW, FindFirstFile, FindNextFile, FILE_ATTRIBUTE_DIRECTORY, GetFileAttributesW, GetFileAttributesExW, INVALID_FILE_ATTRIBUTES, WIN32_FILE_ATTRIBUTE_DATA
#include <handleapi.h> // INVALID_HANDLE_VALUE
#include <stringapiset.h> // MultiByteToWideChar, WideCharToMultiByte
#include <timezoneapi.h> // FileTimeToSystemTime
#include <WinDef.h> // /!\ This is required by winbase.h which for whatever reason doesn't include it
#include <winbase.h> // CopyFileW, MoveFileW, SetCurrentDirectory

#pragma region TYPES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

enum e_result
{
	RESULT_SUCCESS      = 1,
	RESULT_FAILURE      = 0,
	RESULT_TRUE         = RESULT_SUCCESS,
	RESULT_FALSE        = RESULT_FAILURE,
	RESULT_EXIT_SUCCESS = 0,
	RESULT_EXIT_FAILURE = 1
};

typedef struct s_params
{
	char const * const picturesPath;
	char const * const destinationPath;
	bool const         move;
} s_params;

#pragma endregion

#pragma region MACRO - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// How to use: WCHAR* utf16 = UTF8_TO_UTF16(utf8String);
// It is required that the variable storing the new string has the exact same name (utf16).
#define UTF8_TO_UTF16(utf8) NULL; u32 utf16Len = MultiByteToWideChar(CP_UTF8, 0, utf8, -1, NULL, 0); utf16 = alloca(sizeof(WCHAR) * utf16Len); MultiByteToWideChar(CP_UTF8, 0, utf8, -1, utf16, utf16Len)
// How to use: char* utf8 = UTF16_TO_UTF8(utf16String);
// It is required that the variable storing the new string has the exact same name (utf8).
#define UTF16_TO_UTF8(utf16) NULL; u32 utf8Len = WideCharToMultiByte(CP_UTF8, 0, utf16, -1, NULL, 0, NULL, NULL); utf8 = alloca(utf8Len); WideCharToMultiByte(CP_UTF8, 0, utf16, -1, utf8, utf8Len, NULL, NULL)

#pragma endregion

#pragma region WIN32 WRAPPER UTF8 -> UTF16 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

BOOL
Win32CopyFile(char const * existingFileName, char const * newFileName, BOOL bFailIfExists)
{
	WCHAR* utf16A = NULL;
	u32 utf16ALen = MultiByteToWideChar(CP_UTF8, 0, existingFileName, -1, NULL, 0);
	utf16A = alloca(sizeof(WCHAR) * utf16ALen);
	MultiByteToWideChar(CP_UTF8, 0, existingFileName, -1, utf16A, utf16ALen);

	WCHAR* utf16B = NULL;
	u32 utf16BLen = MultiByteToWideChar(CP_UTF8, 0, newFileName, -1, NULL, 0);
	utf16B = alloca(sizeof(WCHAR) * utf16BLen);
	MultiByteToWideChar(CP_UTF8, 0, newFileName, -1, utf16B, utf16BLen);

	return CopyFileW(utf16A, utf16B, bFailIfExists);
}

BOOL
Win32CreateDirectory(char const * pathName, LPSECURITY_ATTRIBUTES lpSecurityAttributes)
{
	WCHAR* utf16 = UTF8_TO_UTF16(pathName);
	return CreateDirectoryW(utf16, lpSecurityAttributes);
}

HANDLE
Win32CreateFile(char const * fileName, DWORD dwDesiredAccess, DWORD dwShareMode, LPSECURITY_ATTRIBUTES lpSecurityAttributes, DWORD dwCreationDisposition, DWORD dwFlagsAndAttributes, HANDLE hTemplateFile)
{
	WCHAR* utf16 = UTF8_TO_UTF16(fileName);
	return CreateFileW(utf16, dwDesiredAccess, dwShareMode, lpSecurityAttributes, dwCreationDisposition, dwFlagsAndAttributes, hTemplateFile);
}

BOOL
Win32GetFileAttributesEx(char const * fileName, GET_FILEEX_INFO_LEVELS fInfoLevelId, LPVOID lpFileInformation)
{
	WCHAR* utf16 = UTF8_TO_UTF16(fileName);
	return GetFileAttributesExW(utf16, fInfoLevelId, lpFileInformation);
}

DWORD
Win32GetFileAttributes(char const * fileName)
{
	WCHAR* utf16 = UTF8_TO_UTF16(fileName);
	return GetFileAttributesW(utf16);
}

BOOL
Win32MoveFile(char const * existingFileName, char const * newFileName)
{
	WCHAR* utf16A = NULL;
	u32 utf16ALen = MultiByteToWideChar(CP_UTF8, 0, existingFileName, -1, NULL, 0);
	utf16A = alloca(sizeof(WCHAR) * utf16ALen);
	MultiByteToWideChar(CP_UTF8, 0, existingFileName, -1, utf16A, utf16ALen);

	WCHAR* utf16B = NULL;
	u32 utf16BLen = MultiByteToWideChar(CP_UTF8, 0, newFileName, -1, NULL, 0);
	utf16B = alloca(sizeof(WCHAR) * utf16BLen);
	MultiByteToWideChar(CP_UTF8, 0, newFileName, -1, utf16B, utf16BLen);

	return MoveFileW(utf16A, utf16B);
}

BOOL
Win32SetCurrentDirectory(char const * pathName)
{
	WCHAR* utf16 = UTF8_TO_UTF16(pathName);
	return SetCurrentDirectoryW(utf16);
}

#pragma endregion

u32
CountParameterValues(char*const* av)
{
	u32 count = 0;
	while (*av != NULL)
	{
		if ((*av)[0] == '-' && (*av)[1] == '-')
			break;
		++count;
		++av;
	}
	return count;
}

bool
DirectoryPathIsValid(char const * directoryPath)
{
	for (char const * c = directoryPath; *c; ++c)
	{
		if (*c == '\\')
		{
			fprintf(stderr, "Invalid parameter: \"%s\"\n'\\' found in directory path, only '/' are allowed!\n", directoryPath);
			return false;
		}
		else if (*(c + 1) == '\0' && *c != '/')
		{
			fprintf(stderr, "Invalid parameter: \"%s\"\nDirectory path must end with a '/'!\n", directoryPath);					
			return false;
		}
	}
	return true;
}

enum e_result
ParamParse(char const * const * av, struct s_params* params)
{
	char const * paramPicturesPath    = "--pictures_path";
	char const * paramDestinationPath = "--destination_path";
	char const * paramMove            = "--move";
    
	for (u32 i = 0; av[i] != NULL; ++i)
	{
		if (av[i][0] != '-')
		{
			fprintf(stdout, "Unknown parameter: %s\n", av[i]);
			return RESULT_FAILURE;
		}
        
		char const * param = av[i];
		if (strcmp(paramPicturesPath, param) == 0)
		{
			++i;
			*(char const **)&params->picturesPath = av[i];
			if (DirectoryPathIsValid(params->picturesPath) == false)
				return RESULT_FAILURE;

		}
		else if (strcmp(paramDestinationPath, param) == 0)
		{
			++i;
			*(char const **)&params->destinationPath = av[i];
			if (DirectoryPathIsValid(params->destinationPath) == false)
				return RESULT_FAILURE;
		}
		else if (strcmp(paramMove, param) == 0)
		{
			*(bool*)&params->move = true;
		}
		else
		{
			fprintf(stdout, "Unknown parameter: %s\n", param);
			return RESULT_FAILURE;
		}
    }
	return RESULT_SUCCESS;
}

void
ErrorCodeToMessage(char* functionName, int errorCode)
{
	// Retrieve the system error message for the last-error code
	LPVOID errorText;
	if (FormatMessageA(
		// use system message tables to retrieve error text
		FORMAT_MESSAGE_FROM_SYSTEM
		// allocate buffer on local heap for error text
		|FORMAT_MESSAGE_ALLOCATE_BUFFER
		// Important! will fail otherwise, since we're not 
		// (and CANNOT) pass insertion parameters
		|FORMAT_MESSAGE_IGNORE_INSERTS,  
		NULL,    // unused with FORMAT_MESSAGE_FROM_SYSTEM
		errorCode,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPSTR)&errorText,  // output 
		0, // minimum size for output buffer
		NULL) == 0)
	{
		ErrorCodeToMessage("ErrorMessage", GetLastError());
		return;
	}
	fprintf(stderr, "/!\\ [%s] %s", functionName, (char*)errorText);
	LocalFree(errorText);
}

bool
SearchFilter(char const * fileName, u32 fileNameLength)
{
	static u32 l = 3;
	if (fileNameLength < l)
		return false;
	static char const * f[] = {
		"JPG",
		"NEF",
		"MOV"
	};
	u32 offset = fileNameLength - l;
	if (strncmp(fileName + offset, f[0], l) &&
		strncmp(fileName + offset, f[1], l) &&
		strncmp(fileName + offset, f[2], l))
		return false;
	return true;
}

void
RecursiveSearchAndCopy(char const * sourceDirectoryPath, char const * destinationDirectoryPath, bool move)
{
	sz sourceDirectoryPathLength = strlen(sourceDirectoryPath);

	s_file_system_search search = {0};
	if (FileSystemSearch(&search, sourceDirectoryPath, "*", FS_SEARCH_MODE_DIRECTORY, NULL) == FS_SUCCESS)
	{
		char* nextDirectoryPath = malloc(sourceDirectoryPathLength + 1);
		memcpy(nextDirectoryPath, sourceDirectoryPath, sourceDirectoryPathLength + 1);
		u32 i = search.fileCount;
		while (i--)
		{
			nextDirectoryPath = realloc(nextDirectoryPath, sourceDirectoryPathLength + search.fileNameLengths[i] + 2);
			memcpy(nextDirectoryPath + sourceDirectoryPathLength, search.fileNames[i], search.fileNameLengths[i] + 1);
			nextDirectoryPath[sourceDirectoryPathLength + search.fileNameLengths[i]] = '/';
			nextDirectoryPath[sourceDirectoryPathLength + search.fileNameLengths[i]+1] = '\0';
			RecursiveSearchAndCopy(nextDirectoryPath, destinationDirectoryPath, move);
		}
		FileSystemSearchFree(&search);
	}

	u32 previousFileNameLength = 0;
	SYSTEMTIME previousTime = {0};
	char dir1[5]  = {0};
	char dir2[11] = {0};
	char* fullPath = malloc(sourceDirectoryPathLength);
	memcpy(fullPath, sourceDirectoryPath, sourceDirectoryPathLength);
	if (FileSystemSearch(&search, sourceDirectoryPath, "*", FS_SEARCH_MODE_FILE, &SearchFilter) == FS_SUCCESS)
	{
		if (search.fileCount)
			fprintf(stdout, "%s\n", sourceDirectoryPath);

		u32 i = search.fileCount;
		while (i--)
		{
			u32 fileNameLength = search.fileNameLengths[i];
			if (previousFileNameLength < fileNameLength)
			{
				fullPath = realloc(fullPath, sourceDirectoryPathLength + fileNameLength + 1);
				previousFileNameLength = fileNameLength;
			}
			char const * fileName = search.fileNames[i];
			memcpy(fullPath + sourceDirectoryPathLength, fileName, fileNameLength + 1);

			WIN32_FILE_ATTRIBUTE_DATA fileAttributes = {0};
			if (Win32GetFileAttributesEx(fullPath, GetFileExInfoStandard, &fileAttributes))
			{
				SYSTEMTIME time = {0};
				if (FileTimeToSystemTime(&fileAttributes.ftCreationTime, &time) == FALSE)
				{
					ErrorCodeToMessage("FileTimeToSystemTime", GetLastError());
					continue;
				}

				if ((previousTime.wYear  != time.wYear)  ||
					(previousTime.wMonth != time.wMonth) ||
					(previousTime.wDay   != time.wDay))
				{
					previousTime.wYear  = time.wYear;
					previousTime.wMonth = time.wMonth;
					previousTime.wDay   = time.wDay;

					Win32SetCurrentDirectory(destinationDirectoryPath);

					sprintf(dir1, "%hd", time.wYear);
					sprintf(dir2, "%hd-%02hd-%02hd", time.wYear, time.wMonth, time.wDay);

					if (Win32GetFileAttributes(dir1) == INVALID_FILE_ATTRIBUTES)
						Win32CreateDirectory(dir1, NULL);
					Win32SetCurrentDirectory(dir1);

					if (Win32GetFileAttributes(dir2) == INVALID_FILE_ATTRIBUTES)
						Win32CreateDirectory(dir2, NULL);
					Win32SetCurrentDirectory(dir2);
				}

				fprintf(stdout, "\t[%u/%u] %s --> %s%s/%s/%s\n", (search.fileCount - i), search.fileCount, fullPath, destinationDirectoryPath, dir1, dir2, fileName);

				if (move)
				{
					if (Win32MoveFile(fullPath, fileName) == FALSE)
					{
						ErrorCodeToMessage("MoveFile", GetLastError());
						continue;
					}
				}
				else
				{
					if (Win32CopyFile(fullPath, fileName, TRUE) == FALSE)
					{
						ErrorCodeToMessage("CopyFile", GetLastError());
						continue;
					}
				}

				// NOTE: This might not be necessary if the src & dest are on the same drive and we move the file rather than copying
				HANDLE fileHandle = Win32CreateFile(fileName, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_WRITE_ATTRIBUTES, NULL);
				if (fileHandle == INVALID_HANDLE_VALUE)
				{
					ErrorCodeToMessage("CreateFile", GetLastError());
					continue;
				}

				if (SetFileTime(fileHandle, &fileAttributes.ftCreationTime, &fileAttributes.ftLastAccessTime, &fileAttributes.ftLastWriteTime) == FALSE)
				{
					ErrorCodeToMessage("SetFileTime", GetLastError());
				}

				CloseHandle(fileHandle);
			}
			else
			{
				ErrorCodeToMessage("GetFileAttributesEx", GetLastError());
			}
		}
		FileSystemSearchFree(&search);
	}
}

int
wmain(int argc, wchar** wargv)
{
	UINT previousConsoleCodePage = GetConsoleOutputCP();
	SetConsoleOutputCP(CP_UTF8);
	{
		fprintf(stdout, "📷 Photo Transfer v0.1\n");

		char** argv = malloc(sizeof(char*) * (argc + 1));
		for (i32 i = 0; i < argc; ++i)
		{
			char* utf8 = UTF16_TO_UTF8(wargv[i]);
			argv[i] = malloc(sizeof(char) * utf8Len);
			memcpy(argv[i], utf8, utf8Len);
		}
		argv[argc] = NULL;
		
		{
			s_params params = {0};
			if (ParamParse(argv + 1, &params) == RESULT_SUCCESS && params.picturesPath && params.destinationPath)
			{
				fprintf(stdout, "Pictures Path: %s\n"   , params.picturesPath);
				fprintf(stdout, "Destination Path: %s\n", params.destinationPath);
				fprintf(stdout, "Mode: %s\n"            , params.move ? "Move" : "Copy");

				if (Win32SetCurrentDirectory(params.destinationPath))
					RecursiveSearchAndCopy(params.picturesPath, params.destinationPath, params.move);
				else
					ErrorCodeToMessage("SetCurrentDirectory", GetLastError());
			}
			else
			{
				fprintf(stdout, "Usage: PhotoSorter.exe --pictures_path D:/DCIM/ --destination_path C:/Users/JohnDoe/Pictures/ [--move]\n");
			}
		}
		
		for (i32 i = 0; i < argc; ++i)
		{
			free(argv[i]);
		}
		free(argv);
	}
	SetConsoleOutputCP(previousConsoleCodePage);

	return RESULT_EXIT_SUCCESS;
}